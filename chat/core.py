import logging

from network.chatinterface.main import ChatInterface
import socket

logging.basicConfig(level=logging.INFO)
chat_interface = ChatInterface()

if __name__ == '__main__':
    print('Client uid:\n{}'.format(chat_interface.uid))
    chat_interface.start_accepting_connections_thread()
    chat_interface.get_available_users_from_network()
    print(chat_interface.socket_dict.keys())
    while True:
        inp = input('input: ')
        if inp == 'msg':
            uid = input('Enter uid: ')
            msg = input('Enter msg: ')
            chat_interface.check_and_send_message(uid, msg)
        else:
            print(chat_interface.socket_dict.keys(), end='\n\n\n\n')

    # while True:
    #     print(end='\n\n')
    #     print('Available users:')
    #     print(chat_interface.available_users)
    #     print('Available connections:')
    #     print(chat_interface.available_connections_dict, end='\n\n\n')
    #
    #     user = int(input('Enter User: '))
    #     msg = input('Enter message: ')
    #     chat_interface.send_message((socket.gethostname(), user), msg)
