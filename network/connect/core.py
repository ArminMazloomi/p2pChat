import json
import logging
import random
import socket
import threading

_PACKET_SIZE = 4096

MIN_PORT = 3000
MAX_PORT = 8000 - 1
MAX_RETRIES = 50

busy_ports = set()


def get_empty_port():
    logging.debug('busy ports: {}'.format(busy_ports))
    for i in range(MAX_RETRIES):
        candid_port = random.randint(MIN_PORT, MAX_PORT)
        if candid_port not in busy_ports:
            busy_ports.add(candid_port)
            logging.debug('port {} chosen'.format(candid_port))
            return candid_port


def get_tcp_socket(ip, port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((ip, int(port)))
    s.listen(1)
    return s


def start_listening_thread(s):
    t = threading.Thread(target=_start_listening, args=(s,))
    t.start()


def start_listening(s: socket.socket):
    _start_listening(s)


def _start_listening(s: socket.socket):
    while True:
        data = s.recv(_PACKET_SIZE)
        try:
            deser_data = deserialize_packet(data)
            print('Message from {}'.format(deser_data['uid']))
            print(deser_data['msg'], end='\n\n')
        except json.decoder.JSONDecodeError as e:
            logging.debug(e)


def deserialize_packet(data):
    return json.loads(data.decode('utf-8'))
