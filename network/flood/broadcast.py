import random
import socket
import logging
import json

# TODO add to env
_PACKET_SIZE = 4096
UDP_SOCKET_TIMEOUT = 5
MIN_PORT = 8000 + 1
MAX_PORT = 9000
UDP_SENDER_PORT = random.randint(MIN_PORT, MAX_PORT)
BROADCAST_MESSAGE = 'Hello'
UDP_RECEIVER_PORT = 8000
BROADCAST_IP = '255.255.255.255'


def get_broadcast_socket():
    listen_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    listen_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    listen_socket.bind(('', UDP_SENDER_PORT))
    return listen_socket


def receive_responses(s: socket.socket):
    received_addrs = set()
    # TODO closes the socket for good ***bug***
    while True:
        try:
            s.settimeout(UDP_SOCKET_TIMEOUT)
            data, addr = s.recvfrom(_PACKET_SIZE)
            logging.debug(data)
            dec_data = deserialize_broadcast_response_packet(data)
            port = dec_data['port']
            uid = dec_data['uid']
            new_addr = (uid, (addr[0], int(port)))
            logging.debug('received port: {}'.format(new_addr))
            received_addrs.add(new_addr)
        except socket.timeout:
            s.settimeout(None)
            break
    return received_addrs


def serialize_packet(**kwargs):
    return json.dumps(kwargs).encode('utf-8')


def chat_request_network(s: socket.socket, pkt):
    logging.debug('sent broadcast message on port {}'.format(UDP_RECEIVER_PORT))
    s.sendto(pkt, (BROADCAST_IP, UDP_RECEIVER_PORT))


def deserialize_broadcast_response_packet(pkt):
    return json.loads(pkt.decode('utf-8'))
